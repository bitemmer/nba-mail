import smtplib
import requests
import json
import urllib.request
import csv

from config import FROM_EMAIL_ADRESS, EMAIL_PASSWORD, TO_EMAIL_ADRESS
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.mime.base import MIMEBase
from email import encoders


from_addr = FROM_EMAIL_ADRESS
to_addr = TO_EMAIL_ADRESS

url = "http://data.nba.net/10s/prod/v2/20190301/scoreboard.json"

response = urllib.request.urlopen(url)
data = json.loads(response.read())
#content = response.read()
#data = json.loads(content.decode("utf8"))
f = open("scoreboard.csv","w")
c = csv.writer(f)

#c.writerow(["Score","Test"])

for d in data:
    c.writerow(["Score","test"])

msg = MIMEMultipart()
msg["FROM"] = from_addr
msg["TO"] = to_addr
msg["SUBJECT"] = "Mail met bijlage"

body = "Dit is een testmail met bijlage"
msg.attach(MIMEText(body, "plain"))

filename = "scoreboard.csv"
attachment = open(filename, "rb")

part = MIMEBase("application","octet-stream")
part.set_payload((attachment).read())
encoders.encode_base64(part)
part.add_header("content-disposition","attachment; filename= "+filename)

msg.attach(part)
text = msg.as_string()
server = smtplib.SMTP('smtp.gmail.com',587)
server.starttls()
server.login(FROM_EMAIL_ADRESS, EMAIL_PASSWORD)

server.sendmail(FROM_EMAIL_ADRESS,TO_EMAIL_ADRESS,text)
server.quit()

