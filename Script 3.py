import requests
import json
import smtplib

from email.mime.multipart import MIMEMultipart
from config import FROM_EMAIL_ADRESS, EMAIL_PASSWORD, TO_EMAIL_ADRESS
from email.mime.text import MIMEText

from_addr = FROM_EMAIL_ADRESS
to_addr = TO_EMAIL_ADRESS

url = "http://data.nba.net/10s/prod/v2/{{gameDate}}/scoreboard.json"
url_twee = "http://data.nba.net/10s/prod/v2/20190301/scoreboard.json"

r = requests.get(url_twee)
json_data = json.loads(r.text)
#data = r.json()

text = r.text

server = smtplib.SMTP('smtp.gmail.com',587)
server.starttls()
server.login(FROM_EMAIL_ADRESS, EMAIL_PASSWORD)

multipart_mail = MIMEMultipart()
multipart_mail["Subject"] = "Mail"
multipart_mail.attach(MIMEText(text,"plain"))
server.sendmail(from_addr, to_addr, json_data)

#print(text)

