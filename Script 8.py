import smtplib
import requests
import csv

from config import FROM_EMAIL_ADRESS, EMAIL_PASSWORD, TO_EMAIL_ADRESS
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.mime.base import MIMEBase
from email import encoders
from pprint import pprint

from_addr = FROM_EMAIL_ADRESS
to_addr = TO_EMAIL_ADRESS

r = requests.get('http://data.nba.net/10s//prod/v2/2018/teams.json')
response_data = r.json()

data = []

for team in response_data['league']['sacramento']:
    d = {
        'stad': team['city'],
        'volledige_naam': team['fullName'],
        'bijnaam': team['nickname']
    }
    data.append(d)

with open('teams2018_versie2.csv','w') as csvfile:
    fieldnames = ['stad', 'volledige_naam', 'bijnaam']
    writer = csv.DictWriter(csvfile, fieldnames=fieldnames)

    writer.writeheader()

    for team in data:
        writer.writerow(team)

msg = MIMEMultipart()
msg["FROM"] = from_addr
msg["TO"] = to_addr
msg["SUBJECT"] = "Mail met bijlage"

body = "Dit is een testmail met bijlage"
msg.attach(MIMEText(body, "plain"))

filename = 'teams2018_versie2.csv'
attachment = open(filename, "rb")

part = MIMEBase("application","octet-stream")
part.set_payload((attachment).read())
encoders.encode_base64(part)
part.add_header("content-disposition","attachment; filename= "+filename)

msg.attach(part)
text = msg.as_string()
server = smtplib.SMTP('smtp.gmail.com',587)
server.starttls()
server.login(FROM_EMAIL_ADRESS, EMAIL_PASSWORD)

server.sendmail(FROM_EMAIL_ADRESS,TO_EMAIL_ADRESS,text)
server.quit()

