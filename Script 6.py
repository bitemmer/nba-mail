import smtplib
import requests
import json
import urllib.request
import unicodecsv as csv

from config import FROM_EMAIL_ADRESS, EMAIL_PASSWORD, TO_EMAIL_ADRESS
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.mime.base import MIMEBase
from email import encoders


from_addr = FROM_EMAIL_ADRESS
to_addr = TO_EMAIL_ADRESS

url = 'http://data.nba.net/10s//prod/v2/2018/teams.json'

response = urllib.request.urlopen(url)
data = json.loads(response.read())

with open('output.csv','wb') as csv_file:
    writer = csv.writer(csv_file, encoding='utf-8')

    writer.writerow (['city', 'nickname', 'urlName'])

    for row in data:
        city_id = row[0]
        nickname = row[1]
        team = row[2]

        row = [city_id, nickname, team]

        writer.writerow(row)

msg = MIMEMultipart()
msg["FROM"] = from_addr
msg["TO"] = to_addr
msg["SUBJECT"] = "Mail met bijlage"

body = "Dit is een testmail met bijlage"
msg.attach(MIMEText(body, "plain"))

filename = "output.csv"
attachment = open(filename, "rb")

part = MIMEBase("application","octet-stream")
part.set_payload((attachment).read())
encoders.encode_base64(part)
part.add_header("content-disposition","attachment; filename= "+filename)

msg.attach(part)
text = msg.as_string()
server = smtplib.SMTP('smtp.gmail.com',587)
server.starttls()
server.login(FROM_EMAIL_ADRESS, EMAIL_PASSWORD)

server.sendmail(FROM_EMAIL_ADRESS,TO_EMAIL_ADRESS,text)
server.quit()

